redBlueHeat<-function(n, mn = -1, mx = 1) {
	# mn is min value
    # mx is max value
    # n is number of values for one half of color scale
    # returns a subportion of the complete heatmap, matching the values that are actually in the image

	redGreen <- vector(mode = "numeric", length = 2 * n + 1)
    for (i in (1):(n)) {
        green <- hexGreen(i, n)
        redGreen[2 * n + 2 - i] <- paste("#FF", green, "00", sep = "")
        #redGreen[i] <- paste("#", green, "FF00", sep = "")
        redGreen[i] <- paste("#", green, "00FF", sep = "")
    }
    redGreen[n + 1] <- "#FFFF00"
    pos <- getPos(n, mn, mx)
    return(redGreen[(pos[1]):(pos[2])])
}