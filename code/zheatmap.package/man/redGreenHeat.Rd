\name{redGreenHeat}
\alias{redGreenHeat}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
redGreenHeat
}
\description{
Return a subportion of the complete color scale, matching the values that are actually in the corresponding data frame. The color scale is the traditional "red" version of the heated body spectrum for positive values, and a "green" version of the heated body spectrum for negative values.
}
\usage{
redGreenHeat(n, mn = -1, mx = 1)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
 \item{n}{
Number of values for one half of color scale.
}
  \item{mn}{
Minimum value in the data frame.
}
  \item{mx}{
Maximum value in the data frame.
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
Returns a subportion of the complete color scale, matching the values that are actually in the corresponding data frame.
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{hexGreen}}
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
