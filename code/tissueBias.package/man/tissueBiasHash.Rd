\name{tissueBiasHash}
\alias{tissueBiasHash}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
tissueBiasHash
}
\description{
loop through several exprdiffs and several "orders"  returns hash first index is exprdiff, second index is "order", values are lists of biased genes
}
\usage{
tissueBiasHash(tbd, range.thresh.exprdiff = c(3, 6), range.order = c(2, 4))
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{tbd}{
return value of tissueBias.diff()
}
  \item{range.thresh.exprdiff}{
range of values of thresh.exressdiff to loop through
}
  \item{range.order}{
range of values of "order" to loop through
}
}
\value{
returns hash first index is exprdiff, second index is "order", values are lists of genes
}
\author{
Barry Zeeberg
}
\seealso{
\code{\link{tissueBias.diff}}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
