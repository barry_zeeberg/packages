\name{tissueBias.retrieveTissueNameDriver}
\alias{tissueBias.retrieveTissueNameDriver}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
tissueBias.retrieveTissueNameDriver
}
\description{
driver to iteratively invoke tissueBias.retrieveTissueName() for multiple genes
}
\usage{
tissueBias.retrieveTissueNameDriver(tb, tb.rhg, tbd, order = "2")
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{tb}{
return value of tissueBias(), namely a matrix rows of tb are names of tissue types, cols are genes, vals are median expression in that tissue type
}
  \item{tb.rhg}{
return value of tissueBias.retrieveHighGenes()
}
  \item{tbd}{
return value of tissueBias.diff()
}
  \item{order}{
order = "2" means highest minus second highest expression in tb (return value of tissueBias())
}
}
\value{
returns a hash whose components are (1) a vector whose components are the return values of tissueBias.retrieveTissueName() and (2) a histogram of the composite tissue counts
}
\author{
Barry Zeeberg
}
\seealso{
\code{\link{tissueBias}}

\code{\link{tissueBias.retrieveTissueName}}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
