\name{tissueBiasCompare2DB.missingGeneMismatch}
\alias{tissueBiasCompare2DB.missingGeneMismatch}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
tissueBiasCompare2DB.missingGeneMismatch
}
\description{
which mismatched biased genes are missing in ccle or sanger database?
}
\usage{
tissueBiasCompare2DB.missingGeneMismatch(tbdd, genes)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{tbdd}{
%%     ~~Describe \code{tbdd} here~~
}
  \item{genes}{
%%     ~~Describe \code{genes} here~~
}
}
\value{
returns a vector of character strings that are the names of mismatched biased genes that are missing in ccle or sanger database
}
\author{
Barry Zeeberg
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
