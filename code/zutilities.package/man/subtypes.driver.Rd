\name{subtypes.driver}
\alias{subtypes.driver}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
subtypes.driver
}
\description{
returns a matrix of gene expression for user-selected subtypes
}
\usage{
subtypes.driver(db, subtypes, subtypes.thresh = NULL)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{db}{
gene expression database in my databases.package format
}
  \item{subtypes}{
names of tissue subtypes to retain. ignored if !is.null(subtypes.thresh)
}
  \item{subtypes.thresh}{
threshold for auto-selecting subtypes according to the number of instances of a subtype 
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
returns the return value of subtypes.subset()
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}
\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
