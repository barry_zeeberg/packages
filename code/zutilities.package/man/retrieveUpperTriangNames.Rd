\name{retrieveUpperTriangNames}
\alias{retrieveUpperTriangNames}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
retrieveUpperTriangNames
}
\description{
compute matrix whose colnames are c("row", "col", "rownames", "colnames", "correl") characterizing each item in upper triangular region of correlation matrix
}
\usage{
retrieveUpperTriangNames(cor,rut=FALSE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{cor}{
correlation matrix
}
  \item{rut}{
boolean if TRUE then retrieve upper triangular else retrieve full matrix
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
returns matrix whose colnames are c("row", "col", "rownames", "colnames", "correl") characterizing each item in upper triangular region of correlation matrix
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
