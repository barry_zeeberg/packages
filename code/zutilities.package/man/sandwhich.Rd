\name{sandwhich}
\alias{sandwhich}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
sandwhich
}
\description{
convert col indices of w to corresponding colnames(mat). instead of w having column of col indices, it now has col of colnames
}
\usage{ 
sandwhich(mat, w, sep = NULL)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{mat}{
a 2D matrix with rownames and colnames
}
  \item{w}{
result of which(mat,arr.ind=TRUE)
}
  \item{sep}{
character to be use as separator of pasted rownames and colnames. if null, then do not paste rownames and colnames.
}
}
\details{
used in conjunciton with whichone()
}
\value{
a matrix with 2 columns, col1 is rownames and col2 is colnames of original matrix mat, or a character vector containing pasted versions of e.g. col1%col2, where sep had been specified as "%"
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Barry Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
\code{\link{whichone}}
}
\examples{
\dontrun{
mat<-matrix(1:12,nrow=4,ncol=3)
rownames(mat)<-c("a","b","c","d")
colnames(mat)<-c("A","B","C")
mat
w<-which(mat>3,arr.ind=TRUE)
sandwhich(mat,w)
}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
