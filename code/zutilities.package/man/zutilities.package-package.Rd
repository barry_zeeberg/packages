\name{zutilities.package-package}
\alias{zutilities.package-package}
\alias{zutilities.package}
\docType{package}
\title{
zutilities.package-package
}
\description{
some utility programs that are needed by other packages in our suite for processing gene expression profile databases
}
\details{
\tabular{ll}{
Package: \tab zutilities.package\cr
Type: \tab Package\cr
Version: \tab 1.0\cr
Date: \tab 2013-02-23\cr
License: \tab GPL(>=2)\cr
}
~~ An overview of how to use the package, including the most important functions ~~
}
\author{
Author: Barry Zeeberg

Maintainer: Barry Zeeberg <barry@discover.nci.nih.gov>
}
\references{
~~ Literature or other references for background information ~~
}
\keyword{ package }
\seealso{
}
\examples{
}
