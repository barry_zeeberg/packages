\name{subtypes.tab.archived}
\alias{subtypes.tab.archived}
\docType{data}
\title{
subtypes.tab.archived
}
\description{
output of subtypes.tab(VINODH.CCLE) saved in subtypes.tab.archived.RData
}
\usage{data("subtypes.tab.archived")}
\format{
  The format is:
 int [1:24(1d)] 186 180 69 62 61 59 52 44 38 36 ...
 - attr(*, "dimnames")=List of 1
  ..$ : chr [1:24] "lung" "haematopoietic_and_lymphoid_tissue" "central_nervous_system" "skin" ...
}
\details{
%%  ~~ If necessary, more details than the __description__ above ~~
}
\source{
%%  ~~ reference to a publication or URL from which the data were obtained ~~
}
\references{
%%  ~~ possibly secondary sources and usages ~~
}
\examples{
}
\keyword{datasets}
