\name{batch.test}
\alias{batch.test}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
batch.test
}
\description{
run batch test_that on all test1.R, test2.R etc files in package subdirectories of main.dirs
}
\usage{
batch.test(main.dirs = c("/Volumes/Macintosh_HD_2/administrative/manuscripts/pwcg/pwcg.manuscript/packages.pwcg/code"), DONTSKIP = FALSE, FAIL = FALSE, TEST = FALSE)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{main.dirs}{
full path name of the directory containing the package directories
}
  \item{DONTSKIP}{
Boolean if TRUE convert "SKIP<-TRUE" to "SKIP<-FALSE"
}
  \item{FAIL}{
Boolean if TRUE convert "FAIL<-FALSE" to "FAIL<-TRUE"
}
  \item{TEST}{
Boolean if TRUE run test_file()
}
}
\details{
set DONTSKIP, FAIL, TEST to FALSE if you just want to see which test files have SKIP or FAIL set
}
\value{
returns no value but has side effect of running test_file() and printing the results to the screen
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Bary Zeeberg
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
\dontrun{
batch.test("/Volumes/Macintosh_HD_2/administrative/manuscripts/pwcg/gene.filtering.manuscript/packages/code",DONTSKIP=TRUE,TEST=TRUE)

batch.test("/Volumes/Macintosh_HD_2/administrative/manuscripts/pwcg/pwcg.manuscript/packages.pwcg/code",DONTSKIP=TRUE,TEST=TRUE)
}
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
