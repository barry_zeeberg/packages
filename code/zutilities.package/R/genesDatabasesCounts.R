genesDatabasesCounts<-
	function(geneLists,databases) {
		h<-hash()
		#print(c("DBG genesDatabasesCounts databases:",keys(databases)))
		genes<-vector("character")
		geneList<-list.files(geneLists,full.names=TRUE)
		#print(c("DBG genesDatabasesCounts geneList:",geneList))
		for(gl in geneList) {
			
			#print(c("DBG genesDatabasesCounts GL:",gl))
			
			fname<-basename(gl)
			db<-strsplit(fname,"__",fixed=TRUE)[[1]][1] # NOTE THIS STRSPLIT ONLY WORKS WITH CCLE SUBTYPES
			print(fname)
			#print(db)
			print(dim(databases[[db]]))
			g<-scan(gl,"character")
			genes<-union(genes,g)
			}
		lg<-length(genes)
		print(c("LENGTH GENES",lg))
		ld<-length(keys(databases))
		m<-matrix(data=rep(0,lg*ld),nrow=lg,ncol=ld)
		rownames(m)<-sort(genes)
		colnames(m)<-keys(databases)
		
		for(gl in geneList) {
			print(c("DBG genesDatabasesCounts LOOP:",dim(m)))
			print(colnames(m))
			fname<-basename(gl)
			db<-strsplit(fname,"__",fixed=TRUE)[[1]][1]
			
			# for CCLE subtypes need to extract tissue type from eg "BREAST_0.5.txt"
			db<-strsplit(db,"_0",fixed=TRUE)[[1]][1]
			
			
			g<-scan(gl,"character")
			#print(c("DB",db))
			#print(c("CHECK DB",setdiff(db,colnames(m))))
			m[g,db]<-1
			}

		h$m<-m
		h$rs<-sort(rowSums(m),decreasing=TRUE)
		return(h)
		}

